# Using 1Password To Manage SSH Keys

Did you know you can use 1Password to manage your SSH Keys? As a Solutions
Architect at GitLab, I have multiple environments and environment types that I
connect to, with the primary use case being interacting with multiple GitLab
instances via the Git CLI.

## The "Old" Ways

Traditional documentation ([example](https://docs.gitlab.com/ee/user/ssh.html))
has you scampering about in the terminal, generating keys with `ssh-keygen`,
then exporting those pub keys to any target applications.

Downsides to this approach include but aren't limited to:

- Weak/No passphrase on the keys
- Decentralized management of multiple keys, especially affecting multiple
  workstation environments
- No support for biometric MFA (eg. Apple TouchID)

## Enter 1Password

1Password added an [SSH Agent](https://blog.1password.com/1password-ssh-agent/)
feature in early 2022. By passing the SSH requests to 1Password, you can:

- Easily create SSH keys from endpoint applications
- More easily manage your SSH keys
- Use TouchID to validate key usage instead of passphrases

### How Do We Do It?

Configuring the `SSH-Agent` on OS X is very straighforward:

#### Enabling the SSH Agent

1. Open 1Password preferences `cmd + ,`
2. Select `Developer`
3. Check `Use SSH agent`
4. Add the Snippet to your `~/.ssh/config` file

Subsequent requests to connect via SSH should now forward all requests to
1Password

#### Adding SSH Keys to 1Password

Assuming you already have keys generated in your `~./ssh` dir, you can import
those directly into 1Password:

1. Open your 1Password Vault
2. Select `+ New Item`
3. Select `SSH Key`
4. Drag and drop your existing key where it reads `Drag a Private Key file here to import`

#### Configuring Multiple GitLab Environments

Configuring multiple keys for different hosts can be done in your SSH config
file, using `Host` parameters. For example:

```shell
Host *
  IdentityAgent "~/.1password/agent.sock"

# gitlab.com (work)
Host gitlab.com
  HostName gitlab.com
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/work_id_ed25519

# gitlab.com (personal)
Host personal.gitlab.com
  HostName gitlab.com
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/personal_id_ed25519
```

The `IdentityFile` parameters can point to the public keys that can be/have
been downloaded from 1Password and will match and operate similar to using
multiple keys directly from the terminal, meaning you'll need to use the
correct git URL when referencing each endpoint.

Note: This also enables host matching so you can, for example, tie GitHub 🤮
SSH keys to a `HOST` value of github.com.

### Next Steps

- Setup commit signing via 1Password
