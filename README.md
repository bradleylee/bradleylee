# :bow: Hello, I'm Bradley!

Solutions Architect at GitLab.<br>
Gotta go fast. :rocket:

## Find Me

- [GitLab](https://gitlab.com/bradleylee)
- [LinkedIn](https://www.linkedin.com/in/bradley-j-lee)

## How I Work

- [Setup](https://gitlab.com/bradleylee/bradleylee/-/blob/main/osx-setup.md)
